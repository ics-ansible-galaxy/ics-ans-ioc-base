import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ioc_base')


def test_chronyd_running(host):
    service = host.service("chronyd")
    assert service.is_running
    assert service.is_enabled


def test_procserv_installed(host):
    cmd = host.run('/usr/bin/procServ --version')
    assert cmd.stdout.startswith('procServ Process Server')


def test_autostart_ioc_service_enabled(host):
    for name in ('ess-boot.service', 'ioc-master.service', 'procServ-vacuum.timer'):
        assert host.service(name).is_enabled
